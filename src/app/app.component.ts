import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-I18n-ngx';

  constructor(private _translateService:TranslateService)
  {
    this._translateService.setDefaultLang('ar');
  }

  switchLanguage(lang : string)
  {
    this._translateService.use(lang);
  }
}
